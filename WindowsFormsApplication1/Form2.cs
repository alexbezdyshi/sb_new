﻿using System;
using System.Data;
using System.Windows.Forms;
using System.Data.OracleClient;
using System.Data.Common;
using System.Collections;
using System.IO;
using System.Text;
using System.Runtime.InteropServices;
using System.Threading;
using System.Diagnostics;
using System.Collections.Generic;

namespace WindowsFormsApplication1
{
    public partial class Form2 : Form
    {
        public static ArrayList inp = new ArrayList();
        public static ArrayList inp1 = new ArrayList();
        public static ArrayList bd = new ArrayList();
        string days, mounts, years, hours, mins, secs = null;
        string dayn, mountn, yearn, hourn, minn, secn = null;
        int type_p = 1;
        string msisdn_p = null;
        string imei_p = null;
        string stime_p = null;
        string etime_p = null;
        string name_p = null;
        string P_A_CELL_p = null;
        DateTime thisDay = DateTime.Today;
        public Form2()
        {
            InitializeComponent();
            textBox2.Text= Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory);
            comboBox8.SelectedIndex = 0;
            toolTip1.SetToolTip(button4, "Для выбора данных по номерам Базовых Станций используется файл с номерами БС" + Environment.NewLine + "записанныйх в одну строку разделенных \";\"" + Environment.NewLine+"Например:"+Environment.NewLine+ "103;1591;1632");
           
            comboBox1.SelectedIndex = 0;
            comboBox9.SelectedIndex = 0;
            comboBox3.SelectedIndex = thisDay.Month - 1;
            comboBox2.SelectedIndex = thisDay.Month - 1;
            if (thisDay.Year == 2016)
            {
                comboBox4.SelectedIndex = 0;
                comboBox5.SelectedIndex = 0;
            }
            if (thisDay.Year == 2017)
            {
                comboBox4.SelectedIndex = 1;
                comboBox5.SelectedIndex = 1;
            }
            if (thisDay.Year == 2018)
            {
                comboBox4.SelectedIndex = 2;
                comboBox5.SelectedIndex = 2;
            }
            if (thisDay.Year == 2019)
            {
                comboBox4.SelectedIndex = 3;
                comboBox5.SelectedIndex = 3;
            }
            if (thisDay.Year == 2020)
            {
                comboBox4.SelectedIndex = 4;
                comboBox5.SelectedIndex = 4;
            }
            string day = thisDay.Day.ToString();
            string mount = thisDay.Month.ToString();
            if (day.Length == 1)
            {
                day = "0" + day;
            }
            if (mount.Length == 1)
            {
                mount = "0" + mount;
            }
            stime_p = "01." + mount + "." + thisDay.Year.ToString() + " 00:00:00";
            
            comboBox2.SelectedIndex = thisDay.Month - 1;
            etime_p = day + "." + mount + "." + thisDay.Year.ToString() + " 23:59:59";
            name_p = "calls_00_" + mount + thisDay.Year.ToString();
            days = "01";
            dayn = day;
            mounts = mount;
            mountn = mount;
            years = thisDay.Year.ToString();
            yearn = thisDay.Year.ToString();
            hours = "00";
            hourn = "23";
            mins = "00";
            minn = "59";
            secs = "00";
            secn = "59";
            lastyear();
            lastmoun(1);
            lastmoun1(1);
            lastday(1);
            lastday1(1);
        }

        void lastday(int x)
        {
            int lonelyday = DateTime.DaysInMonth(Convert.ToInt32(comboBox4.SelectedItem.ToString()), comboBox3.SelectedIndex + 1);
            comboBox7.Items.Clear();
            if (thisDay.Year == Convert.ToInt32(comboBox4.SelectedItem.ToString()) && comboBox3.SelectedIndex == comboBox3.Items.Count - 1)
            {
                lonelyday = thisDay.Day;
            }
            for (int i = 0; i < lonelyday; i++)
            {
                comboBox7.Items.Add(i + 1);
            }
            if (x == 1)
            {
                comboBox7.SelectedIndex = 0;
            }
            if (x == 2)
            {
                comboBox7.SelectedIndex = 0;
            }
        }
        void lastday1(int x)
        {
            int lonelyday = DateTime.DaysInMonth(Convert.ToInt32(comboBox5.SelectedItem.ToString()), comboBox2.SelectedIndex + 1);
            comboBox6.Items.Clear();
            if (thisDay.Year == Convert.ToInt32(comboBox5.SelectedItem.ToString()) && comboBox2.SelectedIndex == comboBox2.Items.Count - 1)
            {
                lonelyday = thisDay.Day;
            }
            for (int i = 0; i < lonelyday; i++)
            {
                comboBox6.Items.Add(i + 1);
            }
            if (x == 1)
            {
                comboBox6.SelectedIndex = thisDay.Day - 1;
            }
            if (x == 2)
            {
                comboBox6.SelectedIndex = comboBox6.Items.Count - 1;
            }
        }
        void lastmoun(int x)
        {
            int lonelymoun = thisDay.Month;
            if (thisDay.Year>Convert.ToInt32(comboBox4.SelectedItem.ToString()))
            {
                lonelymoun = 12;
            }
            comboBox3.Items.Clear();
            for (int i = 0; i < lonelymoun; i++)
            {
                if (i==0) {
                    comboBox3.Items.Add("Январь");
                }
                if (i == 1)
                {
                    comboBox3.Items.Add("Февраль");
                }
                if (i == 2)
                {
                    comboBox3.Items.Add("Март");
                }
                if (i == 3)
                {
                    comboBox3.Items.Add("Апрель");
                }
                if (i == 4)
                {
                    comboBox3.Items.Add("Май");
                }
                if (i == 5)
                {
                    comboBox3.Items.Add("Июнь");
                }
                if (i == 6)
                {
                    comboBox3.Items.Add("Июль");
                }
                if (i == 7)
                {
                    comboBox3.Items.Add("Август");
                }
                if (i == 8)
                {
                    comboBox3.Items.Add("Сентябрь");
                }
                if (i == 9)
                {
                    comboBox3.Items.Add("Октябрь");
                }
                if (i == 10)
                {
                    comboBox3.Items.Add("Ноябрь");
                }
                if (i == 11)
                {
                    comboBox3.Items.Add("Декабрь");
                }
                
            }
            if (x==1) {
                comboBox3.SelectedIndex = thisDay.Month - 1;
            }
            if (x==2) {
                comboBox3.SelectedIndex = 0;
            }
        }

        void lastmoun1(int x)
        {
            int lonelymoun = thisDay.Month;
            if (thisDay.Year > Convert.ToInt32(comboBox5.SelectedItem.ToString()))
            {
                lonelymoun = 12;
            }
            comboBox2.Items.Clear();
            for (int i = 0; i < lonelymoun; i++)
            {
                if (i == 0)
                {
                    comboBox2.Items.Add("Январь");
                }
                if (i == 1)
                {
                    comboBox2.Items.Add("Февраль");
                }
                if (i == 2)
                {
                    comboBox2.Items.Add("Март");
                }
                if (i == 3)
                {
                    comboBox2.Items.Add("Апрель");
                }
                if (i == 4)
                {
                    comboBox2.Items.Add("Май");
                }
                if (i == 5)
                {
                    comboBox2.Items.Add("Июнь");
                }
                if (i == 6)
                {
                    comboBox2.Items.Add("Июль");
                }
                if (i == 7)
                {
                    comboBox2.Items.Add("Август");
                }
                if (i == 8)
                {
                    comboBox2.Items.Add("Сентябрь");
                }
                if (i == 9)
                {
                    comboBox2.Items.Add("Октябрь");
                }
                if (i == 10)
                {
                    comboBox2.Items.Add("Ноябрь");
                }
                if (i == 11)
                {
                    comboBox2.Items.Add("Декабрь");
                }

            }
            if (x == 1)
            {
                comboBox2.SelectedIndex = thisDay.Month - 1;
            }
            if (x == 2)
            {
                comboBox2.SelectedIndex = 0;
            }
        }
        void lastyear()
        {
            int lonelyyaer = thisDay.Year;
            if (thisDay.Year>2016)
            {
                lonelyyaer = thisDay.Year-2016;
            }
            comboBox4.Items.Clear();
            comboBox5.Items.Clear();
            for (int i = 0; i <= lonelyyaer; i++)
            {
                
                    comboBox4.Items.Add(2016+i);
                comboBox5.Items.Add(2016 + i);

            }
           
                comboBox4.SelectedIndex= comboBox4.Items.Count-1;
            comboBox5.SelectedIndex = comboBox4.Items.Count - 1;

        }
        private void comboBox6_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                etime_p = comboBox6.SelectedItem.ToString() + "." + mountn + "." + yearn + " " + hourn + ":" + minn + ":" + secn;
                dayn = comboBox6.SelectedItem.ToString();
            }
            catch { }
        }

        private void numericUpDown5_ValueChanged(object sender, EventArgs e)
        {
            string zz = numericUpDown5.Value.ToString();
            if (zz.Length == 1)
            {
                zz = "0" + zz;
            }
            etime_p = dayn + "." + mountn + "." + yearn + " " + zz + ":" + minn + ":" + secn;
            hourn = zz;
        }

        private void numericUpDown6_ValueChanged(object sender, EventArgs e)
        {

            string zz = numericUpDown6.Value.ToString();
            if (zz.Length == 1)
            {
                zz = "0" + zz;
            }
            etime_p = dayn + "." + mountn + "." + yearn + " " + hourn + ":" + zz + ":" + secn;
            minn = zz;
        }

        private void numericUpDown4_ValueChanged(object sender, EventArgs e)
        {

            string zz = numericUpDown4.Value.ToString();
            if (zz.Length == 1)
            {
                zz = "0" + zz;
            }
            etime_p = dayn + "." + mountn + "." + yearn + " " + hourn + ":" + minn + ":" + zz;
            secn = zz;
        }
        Microsoft.Office.Interop.Excel.Application ObjExcel = new Microsoft.Office.Interop.Excel.Application();
        SaveFileDialog saveFileDialog1 = new SaveFileDialog();
        class MyTable
        {
            public MyTable(string start_time,
                string msisdn_f,
                string imsi,
                string imei,
                string dealed,
                string duration,
                string lcal,
                string area,
                string cell)
            {
                this.start_time = start_time;
                this.msisdn_f = msisdn_f;
                this.imsi = imsi;
                this.imei = imei;
                this.dealed = dealed;
                this.duration = duration;
                this.lcal = lcal;
                this.area = area;
                this.cell = cell;
               

            }
            public string start_time { get; set; }
            public string msisdn_f { get; set; }
            public string imsi { get; set; }
            public string imei { get; set; }
            public string dealed { get; set; }
            public string duration { get; set; }
            public string lcal { get; set; }
            public string area { get; set; }
            public string cell { get; set; }
           
           
        }
        class CallType
        {
            public CallType(string id_t, string type)
            {
                this.id_t = id_t;
                this.type = type;
            }
            public string id_t { get; set; }
            public string type { get; set; }
            

        }

        class CallLogic
        {
            public CallLogic(string id_t, string adress, string azimut)
            {
                this.id_t = id_t;
                this.adress = adress;
                this.azimut = azimut;
            }
            public string id_t { get; set; }
            public string adress { get; set; }
            public string azimut { get; set; }

        }

        List<MyTable> result = new List<MyTable>(3);
        List<CallType> result1 = new List<CallType>(3);
        List<CallLogic> result2 = new List<CallLogic>(3);
        void save(string _f, int k)
        {
            
               

                try {
                    string[] r = new string[result.Count+1];
                    r[0] = "Время начала звонка;Номер телефона запрашиваемого абонента;Международный идентификатор мобильного абонента;Международный идентификатор мобильного оборудования;Номер вызываемого абонента;Продолжительность вызова;Тип соединения;Идентификатор местонахождения группы Базовых станций;Идентификатор Базовой станции и сектора Базовой станции;Номер Базовой станции;Номер сектора Базовой станции;Азимут;Адрес местонахождения Базовой станции;";
                    for (int i = 0; i < result.Count; i++)
                    {
                    string lcall_t = null;
                    string azimut = null;
                    string adress = null;
                    for (int g=0;g<result1.Count;g++)
                    {
                        if (result[i].lcal==result1[g].id_t)
                        {
                            lcall_t = result1[g].type;
                            break;
                        }
                    }
                  
                    try
                    {
                        for (int t = 0; t < result2.Count; t++)
                        {
                            if ((result[i].cell.Substring(0, result[i].cell.Length - 1)) == result2[t].id_t)
                            {
                                azimut = result2[t].azimut;
                                adress = result2[t].adress;
                                break;
                            }
                        }
                        r[i + 1] = "\"" + result[i].start_time + "\";\"" + result[i].msisdn_f + "\";\"" + result[i].imsi + "\";\"" + result[i].imei + "\";\"" + result[i].dealed + "\";\"" + result[i].duration + "\";\"" + lcall_t + "\";\"" + result[i].area + "\";\"" + result[i].cell + "\";\"" + result[i].cell.Substring(0, result[i].cell.Length - 1) + "\";\"" + result[i].cell.Substring(result[i].cell.Length - 1, 1) + "\";\"" + azimut + "\";\"" + adress + "\";";
                    }
                    catch
                    {
                        r[i + 1] = "\"" + result[i].start_time + "\";\"" + result[i].msisdn_f + "\";\"" + result[i].imsi + "\";\"" + result[i].imei + "\";\"" + result[i].dealed + "\";\"" + result[i].duration + "\";\"" + lcall_t + "\";\"" + result[i].area + "\";\"" + result[i].cell + "\";\"" + "" + "\";\"" + "" + "\";\"" + "" + "\";\"" + "" + "\";";
                    }
                    }
                
                    if (result.Count != 0)
                    {
                        this.textBox1.Invoke(new MethodInvoker(delegate ()
                        {
                            textBox1.Text += "Файл " + _f + " успешно сохранен." + Environment.NewLine;
                        }));
                    if (k == 0)
                    {
                        File.WriteAllLines(textBox2.Text + "\\" + _f + ".csv", r, Encoding.Default);
                    }
                    if (k == 1)
                    {
                        File.WriteAllLines(textBox2.Text + "\\" + _f + ".txt", r, Encoding.Default);
                    }
                }
                    else
                    {
                        this.textBox1.Invoke(new MethodInvoker(delegate ()
                        {
                            textBox1.Text += "Запрос не дал результатов!" + Environment.NewLine;
                        }));
                    }
                    this.textBox1.Invoke(new MethodInvoker(delegate ()
                    {
                        textBox1.SelectionStart = textBox1.Text.Length;
                        textBox1.ScrollToCaret();
                    }));
                }
                catch
                {

                    this.textBox1.Invoke(new MethodInvoker(delegate ()
                    {
                        textBox1.Text += "Запрос не дал результатов!" + Environment.NewLine;
                    }));
                    this.textBox1.Invoke(new MethodInvoker(delegate ()
                    {
                        textBox1.SelectionStart = textBox1.Text.Length;
                        textBox1.ScrollToCaret();
                    }));
                }
            
           
           
        }

        private void maskedTextBox1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                button1_Click(sender, e);
            }

        }

        private void maskedTextBox2_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                button1_Click(sender, e);
            }
        }
        OracleCommand cmd = new OracleCommand();
   

        void AsyncVersion()
        {
            int k = 0;
            string lac = null;
            result.Clear();
            this.comboBox8.Invoke(new MethodInvoker(delegate ()
            {
                k = comboBox8.SelectedIndex;
            }));
            this.comboBox9.Invoke(new MethodInvoker(delegate ()
            {
                lac = comboBox9.Text;
            }));
            this.textBox1.Invoke(new MethodInvoker(delegate () {
                    textBox1.SelectionStart = textBox1.Text.Length;
                    textBox1.ScrollToCaret();
                }));
            string msisdn=null;
            try
            {
                string[] sb = msisdn_p.Split(' ');
                msisdn = "72" + sb[2];
            }
            catch
            {
                msisdn= "null";
            }
            try
                {
                if (type_p==3) {
                    imei_p = maskedTextBox2.Text.Substring(0,maskedTextBox2.Text.Length-1)+"0";
                }
                else
                {
                    imei_p = "null";
                }
                }
                catch
                {
                    
                    imei_p = "null";
                }
            cmd.CommandText = "SELECT * FROM TABLE(bis.lugacom_call_detailing.get_logic_calls)";
            cmd.Connection = Form1.ConnectionToOracle;
            cmd.ExecuteNonQuery();

            using (DbDataReader reader = cmd.ExecuteReader())
            {
                if (reader.HasRows)
                {

                    while (reader.Read())
                    {

                        result1.Add(new CallType(reader.GetValue(0).ToString(), reader.GetValue(1).ToString()));


                    }
                }


            }
            cmd.CommandText = "SELECT * FROM TABLE(bis.lugacom_call_detailing.get_lugacom_bs)";
            cmd.Connection = Form1.ConnectionToOracle;
            cmd.ExecuteNonQuery();

            using (DbDataReader reader = cmd.ExecuteReader())
            {
                if (reader.HasRows)
                {

                    while (reader.Read())
                    {

                        result2.Add(new CallLogic(reader.GetValue(0).ToString(), reader.GetValue(1).ToString(), reader.GetValue(2).ToString()));



                    }
                }
            }
                if (type_p == 1|| type_p == 2|| type_p == 3||type_p==6) {
                cmd.CommandText = "SELECT * FROM TABLE(bis.lugacom_call_detailing.call_detailing(p_msisdn => "+msisdn+",p_start_time => '"+stime_p+"',p_end_time => '"+etime_p+"',p_request_type => "+type_p+",p_imei =>"+imei_p+",p_a_cell => null))";
                using (DbDataReader reader = cmd.ExecuteReader())
                {
                    if (reader.HasRows)
                    {

                        while (reader.Read())
                        {

                            result.Add(new MyTable(reader.GetValue(0).ToString(), reader.GetValue(1).ToString(), reader.GetValue(2).ToString(), reader.GetValue(3).ToString(), reader.GetValue(4).ToString(), reader.GetValue(5).ToString(), reader.GetValue(6).ToString(), reader.GetValue(7).ToString(), reader.GetValue(8).ToString()));
                        }
                    }

                }
            }
                if (type_p == 4)
                {
                string sql = null;
                
                for (int i=0;i<bd.Count;i++) {
                    sql += "SELECT * FROM TABLE(bis.lugacom_call_detailing.call_detailing(p_msisdn => null,p_start_time => '" + stime_p + "', p_end_time => '" + etime_p + "',p_request_type => 4, p_imei => null,p_a_cell => "+ bd[i]+ ",p_a_area => "+lac+"))";
                    if (i!=bd.Count-1)
                    {
                        sql += " UNION ALL ";
                    }
                    
                    }

                cmd.CommandText = sql;
                //int c = 0;
                using (DbDataReader reader = cmd.ExecuteReader())
                {
                    if (reader.HasRows)
                    {

                        while (reader.Read())
                        {
                            result.Add(new MyTable(reader.GetValue(0).ToString(), reader.GetValue(1).ToString(), reader.GetValue(2).ToString(), reader.GetValue(3).ToString(), reader.GetValue(4).ToString(), reader.GetValue(5).ToString(), reader.GetValue(6).ToString(), reader.GetValue(7).ToString(), reader.GetValue(8).ToString()));

                        }
                    }

                }
            }
            if (type_p==5)
            {
                cmd.CommandText = "SELECT * FROM TABLE(bis.lugacom_call_detailing.call_detailing(p_msisdn => null,p_start_time => '"+stime_p+"',p_end_time => '"+etime_p+"',p_request_type => 5,p_imei => null,p_a_cell => null,p_a_area => null,p_imsi => "+maskedTextBox3.Text+"))";
                using (DbDataReader reader = cmd.ExecuteReader())
                {
                    if (reader.HasRows)
                    {

                        while (reader.Read())
                        {

                            result.Add(new MyTable(reader.GetValue(0).ToString(), reader.GetValue(1).ToString(), reader.GetValue(2).ToString(), reader.GetValue(3).ToString(), reader.GetValue(4).ToString(), reader.GetValue(5).ToString(), reader.GetValue(6).ToString(), reader.GetValue(7).ToString(), reader.GetValue(8).ToString()));
                        }
                    }

                }
            }
            

            this.textBox1.Invoke(new MethodInvoker(delegate ()
            {

                textBox1.Text += "Запрос обработан!" + Environment.NewLine;
                textBox1.SelectionStart = textBox1.Text.Length;
                textBox1.ScrollToCaret();
                string[] s1=stime_p.Split(' ');
                string[] s2 = etime_p.Split(' ');
                if (type_p==1)
                {
                    save(msisdn_p+ "_"+s1[0]+"-"+s2[0]+"_msisdn_a", k);//
                    if (result.Count!=0) {
                        MessageBox.Show("Файл \"" + textBox2.Text + "\\" + msisdn_p + "_" + s1[0] + "-" + s2[0] + "_msisdn_a" + "\" успешно сохранен!");
                    }
                    else
                    {
                        MessageBox.Show("Запрос не дал результатов!");
                    }
                            }
                if (type_p == 2)
                {
                    save(msisdn_p + "_" + s1[0] + "-" + s2[0] + "_msisdn_b", k);//
                    if (result.Count != 0)
                    {
                        MessageBox.Show("Файл \"" + textBox2.Text + "\\" + msisdn_p + "_" + s1[0] + "-" + s2[0] + "_msisdn_b" + "\" успешно сохранен!");
                    }
                    else
                    {
                        MessageBox.Show("Запрос не дал результатов!");
                    }
                }
                if (type_p == 3)
                {
                    string d = imei_p + "_" + s1[0] + "-" + s2[0];
                    save(d, k);
                    if (result.Count != 0)
                    {
                        MessageBox.Show("Файл \"" + textBox2.Text + "\\" + d + "\" успешно сохранен!");
                    }
                    else
                    {
                        MessageBox.Show("Запрос не дал результатов!");
                    }
                }
                if (type_p == 4)
                {
                    string d =  "bs_" + s1[0] + "-" + s2[0];
                    save(d, k);
                    if (result.Count != 0)
                    {
                        MessageBox.Show("Файл \"" + textBox2.Text + "\\" + d + "\" успешно сохранен!");
                    }
                    else
                    {
                        MessageBox.Show("Запрос не дал результатов!");
                    }
                }
                if (type_p == 5)
                {
                    string d = "imsi_" + s1[0] + "-" + s2[0]+"_"+ maskedTextBox3.Text;
                    save(d, k);
                    if (result.Count != 0)
                    {
                        MessageBox.Show("Файл \"" + textBox2.Text + "\\" + d + "\" успешно сохранен!");
                    }
                    else
                    {
                        MessageBox.Show("Запрос не дал результатов!");
                    }
                }
                if (type_p == 6)
                {
                    string d = "subsA_OR_subsB_" + s1[0] + "-" + s2[0] + "_" + msisdn_p;
                    save(d, k);
                    if (result.Count != 0)
                    {
                        MessageBox.Show("Файл \"" + textBox2.Text + "\\" + d + "\" успешно сохранен!");
                    }
                    else
                    {
                        MessageBox.Show("Запрос не дал результатов!");
                    }
                }
            }));
                    
                   
                   
          
            this.button1.Invoke(new MethodInvoker(delegate ()
            {
                button1.Enabled = true;

            }));
            this.comboBox1.Invoke(new MethodInvoker(delegate ()
            {
                comboBox1.Enabled = true;
            }));
            this.button5.Invoke(new MethodInvoker(delegate ()
               {
                   button5.Enabled = true;
               }));
        }
        private void button1_Click(object sender, EventArgs e)
        {
            button1.Enabled = false;
            comboBox1.Enabled = false;
            button5.Enabled = false;
            string msisdn = null;
            try
            {
                string[] sb = msisdn_p.Split(' ');
                msisdn = sb[2];
            }
            catch
            {
                msisdn = null;
            }
          
                inp.Clear();
                textBox1.Text += "Запрос обрабатывается..." + Environment.NewLine;
                Thread _th = new Thread(new ThreadStart(AsyncVersion));
                _th.IsBackground = true;
                _th.Start();
      
}

        private void button2_Click(object sender, EventArgs e)
        {
            inp1.Clear();
            bd.Clear();
            P_A_CELL_p = "";
            OpenFileDialog openFileDialog1 = new OpenFileDialog();

            openFileDialog1.InitialDirectory = "c:\\";
            openFileDialog1.Filter = "txt files (*.txt)|*.txt";
            openFileDialog1.FilterIndex = 2;
            openFileDialog1.RestoreDirectory = true;
            string cell = null;
            int cell_c = 0;
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                inp1.AddRange(File.ReadAllLines(openFileDialog1.FileName, Encoding.Default));
                string str = inp1[0].ToString().Replace(" ", string.Empty);
                string[] sb = str.Split(';');

                for (int m = 0; m < sb.Length; m++)
                {
                    bool k = false;
                    for (int u=0;u<bd.Count;u++) {
                        if (sb[m] ==bd[u].ToString())
                        {
                            k = true;
                            break;
                        }
                        
                        }
                    if (k==false)
                    {
                        if (sb[m] == "")
                        {

                        }
                        else
                        {
                            bd.Add(sb[m]);
                        }
                            
                    }
                   
                    }
                }
            for (int n = 0; n < bd.Count; n++)
            {
                if (n == bd.Count - 1)
                {
                    P_A_CELL_p += bd[n];
                }
                else
                {
                    P_A_CELL_p += bd[n] + ",";
                }
            }

            textBox1.Text += "Файл  загружен!" + Environment.NewLine;
            textBox1.SelectionStart = textBox1.Text.Length;
            textBox1.ScrollToCaret();
        }

        private void pictureBox2_MouseEnter(object sender, EventArgs e)
        {
            
        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {

                int z = comboBox2.SelectedIndex + 1;
                string zz = z.ToString();
                if (zz.Length == 1)
                {
                    zz = "0" + zz;
                }
                //name_p = "calls_00_" + zz + years;
                //stime_p = days + "." + zz + "." + years + " " + hours + ":" + mins + ":" + secs;
                mountn = zz.ToString();
                etime_p = dayn + "." + mountn + "." + yearn + " " + hourn + ":" + minn + ":" + secn;
                //mounts = zz.ToString();
                //comboBox2.SelectedIndex = comboBox3.SelectedIndex;
                
                lastday1(2);
            }
            catch { }
        }

        private void comboBox5_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {

                //name_p = "calls_00_" + mounts + comboBox4.SelectedItem.ToString();
                stime_p = days + "." + mounts + "." + comboBox4.SelectedItem.ToString() + " " + hours + ":" + mins + ":" + secs;
                etime_p = dayn + "." + mountn + "." + comboBox5.SelectedItem.ToString() + " " + hourn + ":" + minn + ":" + secn;
                years = comboBox4.SelectedItem.ToString();
                yearn = comboBox5.SelectedItem.ToString();
                //comboBox5.SelectedIndex = comboBox4.SelectedIndex;
                //mountn = mounts;
                lastmoun1(2);
                lastday1(2);
            }
            catch { }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {
                textBox2.Text = folderBrowserDialog1.SelectedPath;
            }
        }

        private void Form2_FormClosed(object sender, FormClosedEventArgs e)
        {

            ObjExcel.Quit();
            Marshal.ReleaseComObject(ObjExcel);
            try {
                foreach (Process proc in Process.GetProcessesByName("EXCEL"))
                {
                    proc.Kill();
                }
            }
            catch
            {

            }
            Application.Exit();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBox1.SelectedIndex == 0)
            {
                type_p = 1;
                maskedTextBox1.Enabled = true;
                maskedTextBox2.Enabled = false;
                button2.Enabled = false;
                label2.Enabled = true;
                label3.Enabled = false;
                label4.Enabled = false;
                button4.Enabled = false;
                imei_p = null;
                msisdn_p = maskedTextBox1.Text;
                label17.Enabled = false;
                comboBox9.Enabled = false;
                label18.Enabled = false;
                maskedTextBox3.Enabled = false;


            }
            if (comboBox1.SelectedIndex == 1)
            {
                type_p = 2;
                maskedTextBox1.Enabled = true;
                maskedTextBox2.Enabled = false;
                button2.Enabled = false;
                label2.Enabled = true;
                label3.Enabled = false;
                label4.Enabled = false;
                button4.Enabled = false;
                imei_p = null;
                msisdn_p = maskedTextBox1.Text;
                label17.Enabled = false;
                comboBox9.Enabled = false;
                label18.Enabled = false;
                maskedTextBox3.Enabled = false;


            }
            if (comboBox1.SelectedIndex == 2)
            {
                type_p = 3;
                maskedTextBox1.Enabled = false;
                maskedTextBox2.Enabled = true;
                button2.Enabled = false;
                label2.Enabled = false;
                label3.Enabled = true;
                label4.Enabled = false;
                button4.Enabled = false;
                msisdn_p = null;
                imei_p = maskedTextBox2.Text;
                label17.Enabled = false;
                comboBox9.Enabled = false;
                label18.Enabled = false;
                maskedTextBox3.Enabled = false;


            }
            if (comboBox1.SelectedIndex == 3)
            {
                type_p = 4;
                maskedTextBox1.Enabled = false;
                maskedTextBox2.Enabled = false;
                button2.Enabled = true;
                label2.Enabled = false;
                label3.Enabled = false;
                label4.Enabled = true;
                button4.Enabled = true;
                label17.Enabled = true;
                comboBox9.Enabled = true;
                label18.Enabled = false;
                maskedTextBox3.Enabled = false;

            }
            if (comboBox1.SelectedIndex == 4)
            {
                type_p = 5;            
                maskedTextBox1.Enabled = true;
                maskedTextBox2.Enabled = false;
                button2.Enabled = false;
                label2.Enabled = true;
                label3.Enabled = false;
                label4.Enabled = false;
                button4.Enabled = false;
                imei_p = null;
                msisdn_p = maskedTextBox1.Text;
                label17.Enabled = false;
                comboBox9.Enabled = false;
                maskedTextBox1.Enabled = false;
                label2.Enabled = false;
                label18.Enabled = true;
                maskedTextBox3.Enabled = true;

            }
            if (comboBox1.SelectedIndex == 5)
            {
                type_p = 6;
                maskedTextBox1.Enabled = true;
                maskedTextBox2.Enabled = false;
                button2.Enabled = false;
                label2.Enabled = true;
                label3.Enabled = false;
                label4.Enabled = false;
                button4.Enabled = false;
                imei_p = null;
                msisdn_p = maskedTextBox1.Text;
                label17.Enabled = false;
                comboBox9.Enabled = false;
                label18.Enabled = false;
                maskedTextBox3.Enabled = false;


            }
        }

        private void maskedTextBox1_KeyUp(object sender, KeyEventArgs e)
        {
            msisdn_p = maskedTextBox1.Text;
        }

        private void comboBox7_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                stime_p = comboBox7.SelectedItem.ToString() + "." + mounts + "." + years + " " + hours + ":" + mins + ":" + secs;
                days = comboBox7.SelectedItem.ToString();
            }
            catch { }
        }

        private void comboBox3_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {

                int z = comboBox3.SelectedIndex + 1;
                string zz = z.ToString();
                if (zz.Length == 1)
                {
                    zz = "0" + zz;
                }
                //name_p = "calls_00_" + zz + years;
                stime_p = days + "." + zz + "." + years + " " + hours + ":" + mins + ":" + secs;
                //etime_p = dayn + "." + zz + "." + yearn + " " + hourn + ":" + minn + ":" + secn;
                mounts = zz.ToString();
                //comboBox2.SelectedIndex = comboBox3.SelectedIndex;
                //mountn = zz.ToString();
                lastday(2);
            }
            catch { }
        }

        private void comboBox4_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {

                //name_p = "calls_00_" + mounts + comboBox4.SelectedItem.ToString();
                stime_p = days + "." + mounts + "." + comboBox4.SelectedItem.ToString() + " " + hours + ":" + mins + ":" + secs;
                etime_p = dayn + "." + mountn + "." + comboBox5.SelectedItem.ToString() + " " + hourn + ":" + minn + ":" + secn;
                years = comboBox4.SelectedItem.ToString();
                yearn = comboBox5.SelectedItem.ToString();
                //comboBox5.SelectedIndex = comboBox4.SelectedIndex;
                mountn = mounts;
                lastmoun(2);
                lastday(2);
            }
            catch { }
        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            string zz = numericUpDown1.Value.ToString();
            if (zz.Length == 1)
            {
                zz = "0" + zz;
            }
            stime_p = days + "." + mounts + "." + years + " " + zz + ":" + mins + ":" + secs;
            hours = zz;
        }

        private void numericUpDown2_ValueChanged(object sender, EventArgs e)
        {
            string zz = numericUpDown2.Value.ToString();
            if (zz.Length == 1)
            {
                zz = "0" + zz;
            }
            stime_p = days + "." + mounts + "." + years + " " + hours + ":" + zz + ":" + secs;
            mins = zz;
        }

        private void numericUpDown3_ValueChanged(object sender, EventArgs e)
        {
            string zz = numericUpDown3.Value.ToString();
            if (zz.Length == 1)
            {
                zz = "0" + zz;
            }
            stime_p = days + "." + mounts + "." + years + " " + hours + ":" + mins + ":" + zz;
            secs = zz;
        }
    }
}